package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/jaytaylor/html2text"
	"gitlab.com/prarit/rhstatus/cmd"
	"gitlab.com/prarit/rhstatus/internal/config"
	lab "gitlab.com/prarit/rhstatus/internal/gitlab"
	"gitlab.com/prarit/rhstatus/internal/jira"
)

var version = "master"

func main() {
	cmd.Version = version

	// check the version on GitLab Pages
	resp, _ := http.Get("https://prarit.gitlab.io/rhstatus/version.html")
	defer resp.Body.Close()

	strbody, _ := ioutil.ReadAll(resp.Body)
	body := strings.Replace(string(strbody), "\n", "<br>\n", -1)
	html2textOptions := html2text.Options{
		PrettyTables: true,
		OmitLinks:    true,
	}
	body, _ = html2text.FromString(body, html2textOptions)

	verExp, _ := regexp.Compile("version=.*")
	v := verExp.FindAllString(body, -1)
	verStrings := strings.Split(v[0], "=")
	if strings.TrimSpace(verStrings[1]) != strings.TrimSpace(cmd.Version) {
		fmt.Printf("WARNING: This version of rhstatus (%s) is out-of-date.  It may not return correct data.  An update to %s is recommended.\n", strings.TrimSpace(cmd.Version), strings.TrimSpace(verStrings[1]))
	}

	initSkipped := skipInit()
	if !initSkipped {
		glhost, gluser, gltoken, jtoken, jurl := config.LoadMainConfig()

		// initialize GitLab
		lab.Init(glhost, gluser, gltoken)

		// initialize Jira
		jira.Init(jtoken, jurl)
		err := jira.CheckJiraAccess(jurl)
		if err != nil {
			fmt.Println("Jira access has failed.  This is likely due to a misconfigured Jira Token or Jira URL. Please see the project README.md for instructions on resetting your configuration.")
			log.Fatal(err)
		}

	}
	cmd.Execute(initSkipped)
}

func skipInit() bool {
	nArgs := len(os.Args)
	if nArgs <= 1 {
		return false
	}
	switch os.Args[nArgs-1] {
	case "-h", "--help":
		return true
	}
	switch os.Args[1] {
	case "-h", "--help", "help":
		return true
	default:
		return false
	}
}
