package jira

import (
	"fmt"
	"log"
	"strings"

	jira "github.com/andygrunwald/go-jira"
	"github.com/mitchellh/mapstructure"
)

var (
	rhjc *jira.Client // The actual initalized client
	err  error
)

func Init(jtoken string, jurl string) {

	// Create a BasicAuth Transport object
	tp := jira.BearerAuthTransport{
		Token: jtoken,
	}
	// Create a new Jira Client
	rhjc, err = jira.NewClient(tp.Client(), jurl)
	if err != nil {
		log.Fatal(err)
	}
}

func CheckJiraAccess(jurl string) error {
	me, _, err := rhjc.User.GetSelf()
	if err != nil {
		return err
	}
	if me.DisplayName == "" {
		log.Fatalf("Could not get Red Hat Jira data from %s.  Verify that your Red Hat Jira Token is valid.\n", jurl)
	}
	return nil
}

func GetClient() *jira.Client {
	return rhjc
}

func GetIssue(issueID string) (*jira.Issue, error) {
	issue, _, err := rhjc.Sprint.GetIssue(issueID, nil)
	if err != nil {
		return issue, err
	}
	return issue, nil
}

func GetTargetVersion(issue *jira.Issue) (retStr string, err error) {
	type version struct {
		mapArchived bool
		Description string
		ID          string
		Name        string
		released    bool
		self        string
	}

	fmt.Println("GetTargetVersion: Are you sure this is the field you want?  Use GetFixVersion() instead.")

	if issue.Fields.Unknowns["customfield_12323140"] == nil {
		return "", fmt.Errorf("Jira Target Version is not set\n")
	}
	var versions []string
	for _, entry := range issue.Fields.Unknowns["customfield_12323140"].([]interface{}) {
		var u version
		err = mapstructure.Decode(entry, &u)
		if err != nil {
			log.Fatal(err)
		}
		versions = append(versions, u.Name)
	}
	retStr = strings.Join(versions, ",")
	return retStr, nil
}

func GetFixVersions(issue *jira.Issue) (retStr string, err error) {
	var versions []string
	for _, f := range issue.Fields.FixVersions {
		versions = append(versions, f.Name)
	}
	retStr = strings.Join(versions, ",")
	return retStr, nil
}

func getValueFromOption(issue *jira.Issue, fieldID string) (string, error) {
	type option struct {
		Value    string
		Id       string
		Disabled bool // this means disabled in search
		Self     string
	}

	switch issue.Fields.Unknowns[fieldID].(type) {
	case string:
		return issue.Fields.Unknowns[fieldID].(string), nil
	default:
		var u option
		err = mapstructure.Decode(issue.Fields.Unknowns[fieldID], &u)
		if err != nil {
			return "", err
		}
		return u.Value, nil
	}

	return "", nil
}

func GetDocsImpact(issue *jira.Issue) (string, error) {
	return getValueFromOption(issue, "customfield_12319286")
}

func GetDevTargetMilestone(issue *jira.Issue) (string, error) {
	return getValueFromOption(issue, "customfield_12318141")
}

func GetInternalTargetMilestone(issue *jira.Issue) (string, error) {
	return getValueFromOption(issue, "customfield_12321040")
}

func CheckGitLabLink(issue *jira.Issue, webURL string) bool {
	// get Testable Builds (this seems to work right now.  In the future this may have to be the entire body)
	if strings.Contains(fmt.Sprintf("%v", issue.Fields.Unknowns["customfield_12321740"]), webURL) {
		return true
	}
	return false
}

func GetPreliminaryTesting(issue *jira.Issue) (string, error) {
	return getValueFromOption(issue, "customfield_12321540")
}

func GetDevTargetEnd(issue *jira.Issue) (string, error) {
	return getValueFromOption(issue, "customfield_12322148")
}

func GetTargetEnd(issue *jira.Issue) (string, error) {
	return getValueFromOption(issue, "customfield_12313942")
}

func getUserString(issue *jira.Issue, fieldID string) (string, error) {
	var u jira.User

	err := mapstructure.Decode(issue.Fields.Unknowns[fieldID], &u)
	if err != nil {
		log.Fatal(err)
	}
	if u.EmailAddress != "" {
		return fmt.Sprintf("%s <%s>", u.Name, u.EmailAddress), nil
	}
	return fmt.Sprintf("%s", u.Name), nil
}

func GetQAContactInformation(issue *jira.Issue) (string, error) {
	return getUserString(issue, "customfield_12315948")
}
