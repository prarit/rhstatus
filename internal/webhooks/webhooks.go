// This package provides the labels.yaml from https://gitlab.com/cki-project/kernel-workflow/
package webhooks

import (
	lab "gitlab.com/prarit/rhstatus/internal/gitlab"
	"gopkg.in/yaml.v2"
)

type Label struct {
	Name         string `name`
	Color        string `color`
	Description  string `description`
	DevAction    string `devaction`
	Regex        bool   `regex,omitempty`
	RegexFieldID int    `regex_field_id,omitempty`
}

type Labels struct {
	Labels []Label `labels`
}

func GetWebhooksLabelDefinitions() (Labels, error) {
	definitions, err := lab.FileGet("cki-project/kernel-workflow", "utils/labels.yaml", "main")
	if err != nil {
		return Labels{}, err
	}

	var defs Labels
	err = yaml.Unmarshal([]byte(definitions), &defs)
	if err != nil || len(defs.Labels) == 0 {
		return Labels{}, err
	}

	return defs, nil
}
