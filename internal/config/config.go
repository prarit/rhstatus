package config

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"strings"
	"syscall"
	"time"

	"github.com/fatih/color"
	"github.com/spf13/viper"
	gitlab "github.com/xanzy/go-gitlab"
	"github.com/zalando/go-keyring"
	"golang.org/x/crypto/ssh/terminal"
)

const defaultgitlabhost = "https://gitlab.com"
const defaultjiraurl = "https://issues.redhat.com"

var (
	// MainConfig represents the loaded config
	MainConfig  *viper.Viper
	keyringUser string = "rhstatus"
)

// saveToken saves a token into the keyring buffer.  The tokenType is the
// "name" of the service.  For example, tokenType = "bugzilla" results in a
// keyring password for 'rhstatus-user' on 'bugzilla'
func saveToken(tokenType string, token string, confpath string) {
	configString := fmt.Sprintf("core.%stoken", tokenType)

	err := keyring.Set(tokenType, keyringUser, token)
	if err != nil {
		log.Printf("Warning: Unable to save %s token in keyring.  Token is saved as clear text in %s.\n", tokenType, confpath)
		MainConfig.Set(configString, token)
		return
	}
	MainConfig.Set(configString, "rhstatus-keyring-"+tokenType)
	MainConfig.WriteConfig()
}

func jiraInit(reader *bufio.Reader, confpath string) {
	var jiraurl string
	var err error

	if MainConfig.GetString("core.jiraurl") == "" {
		fmt.Printf("\nEnter Jira host (default: %s): ", defaultjiraurl)
		jiraurl, err = reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		jiraurl = strings.TrimSpace(jiraurl)
		if jiraurl == "" {
			jiraurl = defaultjiraurl
		}
	} else {
		// Required to correctly write config
		jiraurl = MainConfig.GetString("core.jiraurl")
	}
	MainConfig.Set("core.jiraurl", jiraurl)

	jiratoken, jiraloadToken, err := readToken("jira", "https:issues.redhat.com (click your avatar, select Profile, and create a token)", *reader)
	if err != nil {
		log.Fatal(err)
	}

	if jiratoken != "" {
		saveToken("jira", jiratoken, confpath)
	} else if jiraloadToken != "" {
		MainConfig.Set("core.jiraloadtoken", jiraloadToken)
		MainConfig.WriteConfig()
	}
}

// New prompts the user for the default config values to use with rhstatus, and save
// them to the provided confpath (default: ~/.config/rhstatus.toml)
func New(confpath string, r io.Reader) error {
	var (
		reader                                   = bufio.NewReader(r)
		gitlabhost, gitlabtoken, gitlabloadToken string
		err                                      error
	)

	// Set GitLab config (host, token, user)

	confpath = path.Join(confpath, "rhstatus.toml")
	if MainConfig.GetString("core.gitlabhost") == "" {
		fmt.Printf("Enter GitLab host (default: %s): ", defaultgitlabhost)
		gitlabhost, err = reader.ReadString('\n')
		gitlabhost = strings.TrimSpace(gitlabhost)
		if err != nil {
			return err
		}
		if gitlabhost == "" {
			gitlabhost = defaultgitlabhost
		}
	} else {
		// Required to correctly write config
		gitlabhost = MainConfig.GetString("core.gitlabhost")
	}

	MainConfig.Set("core.gitlabhost", gitlabhost)

	tokenURL, err := url.Parse(MainConfig.GetString("core.gitlabhost"))
	if err != nil {
		return err
	}
	tokenURL.Path = "-/profile/personal_access_tokens"

	gitlabtoken, gitlabloadToken, err = readToken("gitlab", tokenURL.String(), *reader)
	if err != nil {
		return err
	}

	if gitlabtoken != "" {
		saveToken("gitlab", gitlabtoken, confpath)
	} else if gitlabloadToken != "" {
		MainConfig.Set("core.gitlabloadtoken", gitlabloadToken)
	}

	bugzillatoken, bugzillaloadToken, err := readToken("bugzilla", "https://bugzilla.redhat.com/userprefs.cgi?tab=apikey", *reader)
	if err != nil {
		return err
	}

	if bugzillatoken != "" {
		saveToken("bugzilla", bugzillatoken, confpath)
	} else if bugzillaloadToken != "" {
		MainConfig.Set("core.bugzillaloadtoken", bugzillaloadToken)
	}

	jiraInit(reader, confpath)

	// set default palette
	MainConfig.Set("colors.green", "green")
	MainConfig.Set("colors.grey", "grey")
	MainConfig.Set("colors.red", "red")
	MainConfig.Set("colors.white", "white")
	MainConfig.Set("colors.yellow", "yellow")
	MainConfig.Set("colors.BLUE", "BLUE")

	if err := MainConfig.WriteConfigAs(confpath); err != nil {
		return err
	}
	fmt.Printf("\nConfig saved to %s\n", confpath)

	err = MainConfig.ReadInConfig()
	if err != nil {
		log.Fatal(err)
		UserConfigError()
	}
	return nil
}

var readToken = func(tokenType string, tokenURL string, reader bufio.Reader) (string, string, error) {
	var loadToken string

	fmt.Printf("\nCreate a %s token here: %s\n", tokenType, tokenURL)
	fmt.Printf("Enter %s token, or leave blank to provide a command to provide the token: ", tokenType)
	byteToken, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return "", "", err
	}
	if strings.TrimSpace(string(byteToken)) == "" {
		fmt.Printf("\nEnter command to read the token:")
		loadToken, err = reader.ReadString('\n')
		if err != nil {
			return "", "", err
		}
	}

	if strings.TrimSpace(string(byteToken)) == "" && strings.TrimSpace(loadToken) == "" {
		log.Fatalf("Error: No %s token provided", tokenType)
	}
	return strings.TrimSpace(string(byteToken)), strings.TrimSpace(loadToken), nil
}

func GetGitLabUser(host, token string) string {
	user := MainConfig.GetString("core.gitlabuser")
	if user != "" {
		return user
	}

	lab, _ := gitlab.NewClient(token, gitlab.WithHTTPClient(&http.Client{}), gitlab.WithBaseURL(host+"/api/v4"))
	u, _, err := lab.Users.CurrentUser()
	if err != nil {
		log.Println(err)
		UserConfigError()
	}

	MainConfig.Set("core.gitlabuser", u.Username)
	MainConfig.WriteConfig()
	MainConfig.Set("core.gitlabemail", u.Email)
	MainConfig.WriteConfig()
	fmt.Printf("Email and username saved as %s and %s\n", u.Email, u.Username)

	return u.Username
}

func GetToken(tokenType string) string {
	token := MainConfig.GetString("core." + tokenType + "token")
	if token == "" && MainConfig.GetString("core."+tokenType+"loadtoken") != "" {
		// args[0] isn't really an arg ;)
		args := strings.Split(MainConfig.GetString("core."+tokenType+"loadtoken"), " ")
		_token, err := exec.Command(args[0], args[1:]...).Output()
		if err != nil {
			log.Println(err)
			UserConfigError()
		}
		token = string(_token)
		// tools like pass and a simple bash script add a '\n' to
		// their output which confuses the bugzilla WebAPI
		if token[len(token)-1:] == "\n" {
			token = strings.TrimSuffix(token, "\n")
		}
		return token
	}

	if token != "rhstatus-keyring-"+tokenType && token != "" {
		// Token is clear text and INSECURE.  Attempt to save it.
		// Hopefully this annoys users enough to make sure their keyring
		// is functional.
		saveToken(tokenType, token, MainConfig.ConfigFileUsed())
		// If saveToken "succeeds" then this code will not be executed again
		return token
	}

	token, err := keyring.Get(tokenType, keyringUser)
	if err != nil {
		if tokenType != "jira" {
			log.Fatalf("Unable to download %s password from keyring. Hint: Use seahorse to see if the token was saved to your keyring.", tokenType)
		}

		// jira tokens were added later on, so to stay backwards compatible just ask
		// for a 'new' jira token and url
		reader := bufio.NewReader(os.Stdin)
		jiraInit(reader, MainConfig.ConfigFileUsed())
		token = GetToken("jira")
	}

	return token
}

func GetSubscribedLabels() []string {
	// config.GetString will return an empty string and not nil so we have
	// to check the return first
	labels := MainConfig.GetString("core.subscribedlabels")
	if labels != "" {
		return strings.Split(labels, ",")
	}
	return nil
}

func GetEmail() string {
	return MainConfig.GetString("core.gitlabemail")
}

var rhStatusConfigPath string

func GetHolidays() []time.Time {
	// These files are created from
	// https://source.redhat.com/career/rewards-portfolio/benefits/benefits_wiki
	// and are a copy of the dates.  They are in "Friday, January 1, 2021" format.

	file, err := os.Open(rhStatusConfigPath + "/holidays")
	if err != nil {
		var red = color.New(color.FgRed).SprintFunc()
		fmt.Println(red("WARNING: ~/.config/rhstatus/holidays does not exist.  Deadline"))
		fmt.Println(red("dates may be incorrect.  See rhstatus' README.md for"))
		fmt.Println(red("information on configuring the holidays file."))
		return nil
	}
	defer file.Close()

	var holidays []time.Time
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		t, err := time.Parse("Monday, January 2, 2006", scanner.Text())
		if err != nil {
			log.Fatal(err)
		}
		holidays = append(holidays, t)
	}
	return holidays
}

// LoadMainConfig loads the ~/.config/rhstatus/rhstatus.toml file
func LoadMainConfig() (GitLabhost string, GitLabuser string, GitLabtoken string, Jiratoken string, JiraURL string) {

	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}

	// Try to find XDG_CONFIG_HOME which is declared in XDG base directory
	// specification and use it's location as the config directory
	confpath := os.Getenv("XDG_CONFIG_HOME")
	if confpath == "" {
		confpath = path.Join(home, ".config")
	}
	rhStatusConfigPath = confpath + "/rhstatus"
	if _, err := os.Stat(rhStatusConfigPath); os.IsNotExist(err) {
		os.MkdirAll(rhStatusConfigPath, 0700)
	}

	MainConfig = viper.New()
	MainConfig.SetConfigName("rhstatus.toml")
	MainConfig.SetConfigType("toml")

	// The local path (aka 'dot slash') does not allow for any
	// overrides from the work tree lab.toml
	MainConfig.AddConfigPath(".")
	MainConfig.AddConfigPath(rhStatusConfigPath)

	if _, ok := MainConfig.ReadInConfig().(viper.ConfigFileNotFoundError); ok {
		// Create a new config
		err := New(rhStatusConfigPath, os.Stdin)
		if err != nil {
			log.Fatal(err)
		}
	}

	if !MainConfig.IsSet("core.gitlabhost") {
		GitLabhost = defaultgitlabhost
	} else {
		GitLabhost = MainConfig.GetString("core.gitlabhost")
	}

	if GitLabtoken = GetToken("gitlab"); GitLabtoken == "" {
		UserConfigError()
	}

	GitLabuser = GetGitLabUser(GitLabhost, GitLabtoken)

	Jiratoken = GetToken("jira")
	JiraURL = MainConfig.GetString("core.jiraurl")

	// currently there is no need for a bugzilla token as the
	// Bugzilla REST API is broken.  Old users, however, may
	// have one configured and that should at least be stored
	// in the user's keyring
	// PRARIT: This call can eventually be removed when the
	// migration to Jira is complete.
	GetToken("bugzilla")

	return GitLabhost, GitLabuser, GitLabtoken, Jiratoken, JiraURL
}

func GetGreen() string {
	green := MainConfig.GetString("colors.green")
	if green == "" {
		MainConfig.Set("colors.green", "green")
		MainConfig.WriteConfig()
		green = "green"
	}
	return green
}

func GetGrey() string {
	grey := MainConfig.GetString("colors.grey")
	if grey == "" {
		MainConfig.Set("colors.grey", "grey")
		MainConfig.WriteConfig()
		grey = "grey"
	}
	return grey
}

func GetRed() string {
	red := MainConfig.GetString("colors.red")
	if red == "" {
		MainConfig.Set("colors.red", "red")
		MainConfig.WriteConfig()
		red = "red"
	}
	return red
}

func GetWhite() string {
	white := MainConfig.GetString("colors.white")
	if white == "" {
		MainConfig.Set("colors.white", "white")
		MainConfig.WriteConfig()
		white = "white"
	}
	return white
}

func GetYellow() string {
	yellow := MainConfig.GetString("colors.yellow")
	if yellow == "" {
		MainConfig.Set("colors.yellow", "yellow")
		MainConfig.WriteConfig()
		yellow = "yellow"
	}
	return yellow
}

func GetBLUE() string {
	BLUE := MainConfig.GetString("colors.BLUE")
	if BLUE == "" {
		MainConfig.Set("colors.BLUE", "BLUE")
		MainConfig.WriteConfig()
		BLUE = "BLUE"
	}
	return BLUE
}

// UserConfigError returns a default error message about authentication
func UserConfigError() {
	fmt.Println("Error: User authentication failed.  This is likely due to a misconfigured Personal Access Token.  Verify the token or token_load config settings before attempting to authenticate.")
	os.Exit(1)
}
