package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	gitconfig "github.com/tcnksm/go-gitconfig"
	"gitlab.com/prarit/rhstatus/internal/git"
	lab "gitlab.com/prarit/rhstatus/internal/gitlab"
	"golang.org/x/crypto/ssh/terminal"
)

func init() {
	// NOTE: Calling SetHelpCommand like this causes helpFunc to be called
	// with correct arguments. If the default cobra help func is used no
	// arguments are passed through and subcommand help breaks.
	rhstatusCmd.PersistentFlags().Bool("no-pager", false, "Do not pipe output into a pager")
	rhstatusCmd.PersistentFlags().Bool("debug", false, "Enable debug logging level")
	rhstatusCmd.PersistentFlags().Bool("quiet", false, "Turn off any sort of logging. Only command output is printed")
	rhstatusCmd.PersistentFlags().Bool("version", false, "Display version")

	// We need to set the logger level before any other piece of code is
	// called, thus we make sure we don't lose any debug message, but for
	// that we need to parse the args from command input.
	err := rhstatusCmd.ParseFlags(os.Args[1:])
	// Handle the err != nil case later
	if err == nil {
		debugLogger, _ := rhstatusCmd.Flags().GetBool("debug")
		quietLogger, _ := rhstatusCmd.Flags().GetBool("quiet")
		if debugLogger && quietLogger {
			log.Fatal("option --debug cannot be combined with --quiet")
		}
	}
}

var Version string

var (
	// Will be updated to upstream in Execute() if "upstream" remote exists
	defaultRemote = ""
	// Will be updated to lab.User() in Execute() if forkedFrom is "origin"
	forkRemote = ""
	termWidth  int
	termHeight int
)

// Try to guess what should be the default remote.
func guessDefaultRemote() string {
	// Allow to force a default remote. If set, return early.
	if config := getMainConfig(); config != nil {
		defaultRemote := config.GetString("core.default_remote")
		if defaultRemote != "" {
			return defaultRemote
		}
	}

	guess := ""

	// defaultRemote should try to always point to the upstream project.
	// Since "origin" may have two different meanings depending on how the
	// user forked the project, thus make "upstream" as the most significant
	// remote.
	// In forkFromOrigin approach, "origin" remote is the one pointing to
	// the upstream project.
	_, err := gitconfig.Local("remote.origin.url")
	if err == nil {
		guess = "origin"
	}
	// In forkToUpstream approach, "upstream" remote is the one pointing to
	// the upstream project
	_, err = gitconfig.Local("remote.upstream.url")
	if err == nil {
		guess = "upstream"
	}

	// But it's still possible the user used a custom name
	if guess == "" {
		// use the remote tracked by the default branch if set
		if remote, err := gitconfig.Local("branch.main.remote"); err == nil {
			guess = remote
		} else if remote, err = gitconfig.Local("branch.master.remote"); err == nil {
			guess = remote
		} else {
			// use the first remote added to .git/config file, which, usually, is
			// the one from which the repo was clonned
			remotesStr, err := git.GetLocalRemotesFromFile()
			if err == nil {
				remotes := strings.Split(remotesStr, "\n")
				// remotes format: remote.<name>.<url|fetch>
				remoteName := strings.Split(remotes[0], ".")[1]
				guess = remoteName
			}
		}
	}

	return guess
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(initSkipped bool) {
	var err error
	// Try to gather remote information if running inside a git tree/repo.
	// Otherwise, skip it, since the info won't be used at all, also avoiding
	// misleading error/warning messages about missing remote.
	if git.InsideGitRepo() {
		defaultRemote = guessDefaultRemote()
		if defaultRemote == "" {
			log.Println("No default remote found")
		}

		// Check if the user fork exists
		_, err = gitconfig.Local("remote." + lab.User() + ".url")
		if err == nil {
			forkRemote = lab.User()
		} else {
			forkRemote = defaultRemote
		}
	}

	outputVersion, _ := rhstatusCmd.Flags().GetBool("version")
	if outputVersion {
		fmt.Println("rhstatus version", Version)
		os.Exit(0)
	}

	termWidth, termHeight, err = terminal.GetSize(sysStdout)
	if err != nil {
		log.Fatal("Terminal type not found.")
	}

	if err = rhstatusCmd.Execute(); err != nil {
		// Execute has already logged the error
		os.Exit(1)
	}
}
