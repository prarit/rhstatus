package cmd

import (
	"encoding/json"
	"log"
	"strconv"
	"strings"
	"time"

	bugzilla "gitlab.com/prarit/rhstatus/internal/bugzilla"
	config "gitlab.com/prarit/rhstatus/internal/config"
)

var bugzillaURL = "https://bugzilla.redhat.com"

func getAPIKey() []byte {
	return []byte(config.GetToken("bugzilla"))
}

func convertToInt(value interface{}) int {
	var (
		intret int
		err    error
	)

	switch value.(type) {
	case int:
		intret = value.(int)
	case string:
		intret, err = strconv.Atoi(strings.TrimSpace(value.(string)))
		if err != nil {
			log.Fatal(err)
		}
	}
	return intret
}

func getBugzillaInfo(bzID interface{}) *bugzilla.Bug {
	bzIDint := convertToInt(bzID)
	bzClient := bugzilla.NewClient(getAPIKey, bugzillaURL)
	bz, err := bzClient.GetBug(bzIDint)
	if err != nil {
		log.Fatal(err, " for BZ=", bzIDint)
	}
	return bz
}

func getBugzillaQAContact(bzID interface{}) (string, string, bool) {
	bzIDint := convertToInt(bzID)
	bzClient := bugzilla.NewClient(getAPIKey, bugzillaURL)
	bz, err := bzClient.GetBug(bzIDint)
	if err != nil {
		log.Fatal(err)
	}

	OtherQA := false
	for _, keyword := range bz.Keywords {
		if keyword == "OtherQA" {
			OtherQA = true
			break
		}
	}

	return bz.QAContactDetail.RealName, bz.QAContactDetail.Email, OtherQA
}

func getBugzillaCustomFields(bzID interface{}, fields []string) (bzdatacf BZdataCF) {
	var customFields BZCustomFields

	bzIDint := convertToInt(bzID)
	bzClient := bugzilla.NewClient(getAPIKey, bugzillaURL)
	raw, err := bzClient.GetCustomFields(bzIDint, fields)
	if err != nil {
		log.Fatal(err)
	}

	if err := json.Unmarshal([]byte(raw), &customFields); err != nil {
		log.Fatal(err)
	}
	bzdatacf = customFields.Bugs[0]
	return bzdatacf
}

func getBZdata(bzVal interface{}) BZdata {
	var (
		bzData BZdata
		err    error
	)

	bz := convertToInt(bzVal)

	bzClient := bugzilla.NewClient(getAPIKey, bugzillaURL)

	bzbug := getBugzillaInfo(bz)

	bzData.b = make(map[string]string)

	bzData.b["status"] = bzbug.Status
	bzData.b["product"] = bzbug.Product

	var flags = []string{"devel_ack", "qa_ack", "release", "exception", "blocker", "pgm_legacy_workflow", "zstream"}
	flagsStatus, err := bzClient.GetFlags(bz, flags)
	if err != nil {
		log.Fatal(err)
	}

	bzData.b["devel_ack"] = flagsStatus[0]
	bzData.b["qa_ack"] = flagsStatus[1]
	bzData.b["release"] = flagsStatus[2]

	// exception, blocker, pgm_legacy_workflow, and zstream return "Not Found" when the
	// flag is unset.  The problem is that the rules engine returns "X" for unset, and
	// then it isn't possible to compare the two.  If the flag is "Not Found" then
	// change it to unset.
	for f := 3; f <= 6; f++ {
		if flagsStatus[f] == "Not Found" {
			flagsStatus[f] = "X"
		}
	}

	bzData.b["exception"] = flagsStatus[3]
	bzData.b["blocker"] = flagsStatus[4]
	bzData.b["pgm_legacy_workflow"] = flagsStatus[5]
	bzData.b["zstream"] = flagsStatus[6]

	var customFields BZdataCF

	customFields = getBugzillaCustomFields(bz, []string{"cf_internal_target_milestone", "cf_dev_target_milestone", "cf_internal_target_release", "cf_zstream_target_release", "cf_deadline", "cf_verified"})

	bzData.b["internal_target_milestone"] = customFields.ITM
	bzData.b["dev_target_milestone"] = customFields.DevTM
	bzData.b["internal_target_release"] = customFields.ITR
	bzData.b["zstream_target_release"] = customFields.ZTR
	bzData.verified = customFields.Verified

	if customFields.Deadline == "" {
		bzData.deadline = time.Date(1971, time.January, 1, 0, 0, 0, 0, time.UTC)
	} else {
		// time format appears to be consistent with RFC3339
		bzData.deadline, err = time.Parse(time.RFC3339, customFields.Deadline)
		if err != nil {
			return bzData
		}
	}

	return bzData
}

func getBZReleasePlusRules(ruleNames []string) (rules BZRule) {
	bzClient := bugzilla.NewClient(getAPIKey, bugzillaURL)
	bzStr, err := bzClient.GetRules(ruleNames)
	if err != nil {
		log.Fatal(err)
	}

	if err := json.Unmarshal(bzStr, &rules); err != nil {
		log.Fatal(err)
	}
	// uncomment these lines for debug
	//j, _ := json.MarshalIndent(rules, "", "    ")
	//fmt.Println(string(j))
	return rules
}
