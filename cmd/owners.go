package cmd

import (
	"log"

	"gitlab.com/prarit/rhstatus/internal/config"
	lab "gitlab.com/prarit/rhstatus/internal/gitlab"
	"gopkg.in/yaml.v2"
)

// PRARIT: TODO need to refactor documentation/scripts to provide this as a package
type NameAndEmail struct {
	Name       string
	Email      string
	Restricted bool
}

type SubSystem struct {
	Subsystem string `subsystem`
	Labels    struct {
		Name              string   `name`
		ReadyForMergeDeps []string `readyForMergeDeps`
		NewLabels         string   `newLabels`
		EmailLabel        string   `emailLabel`
	}
	Status      string         `status`
	Maintainers []NameAndEmail `maintainers`
	Reviewers   []NameAndEmail `reviewers`
	Paths       struct {
		Includes       []string
		IncludeRegexes []string
		Excludes       []string
	}
	Scm         string `scm`
	MailingList string `mailingList`
}

type SubSystems struct {
	SubSys []SubSystem `subsystems`
}

var subSystems SubSystems

func downloadOwners() {

	owners, err := lab.FileGet("redhat/centos-stream/src/kernel/documentation", "info/owners.yaml", "main")
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal([]byte(owners), &subSystems)
	if err != nil || len(subSystems.SubSys) == 0 {
		log.Fatalf("error: %v", err)
	}
}

func getOwnersLabels() ([]string, error) {
	if len(subSystems.SubSys) == 0 {
		downloadOwners()
	}

	email := config.GetEmail()

	var ownersSubsystems []string
	for _, subSystem := range subSystems.SubSys {
		for _, maintainer := range subSystem.Maintainers {
			if maintainer.Email == email {
				ownersSubsystems = append(ownersSubsystems, "Subsystem:"+subSystem.Labels.Name)
			}
		}
		for _, reviewer := range subSystem.Reviewers {
			if reviewer.Email == email {
				ownersSubsystems = append(ownersSubsystems, "Subsystem:"+subSystem.Labels.Name)
			}
		}
	}

	return ownersSubsystems, nil
}
