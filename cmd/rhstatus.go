package cmd

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"

	gojira "github.com/andygrunwald/go-jira"
	"github.com/spf13/cobra"
	gitlab "github.com/xanzy/go-gitlab"
	config "gitlab.com/prarit/rhstatus/internal/config"
	lab "gitlab.com/prarit/rhstatus/internal/gitlab"
	jira "gitlab.com/prarit/rhstatus/internal/jira"
	"gitlab.com/prarit/rhstatus/internal/postfix"
	"gitlab.com/prarit/rhstatus/internal/webhooks"
)

var noRole = "        "

var (
	todoType              string
	todoNumRet            string
	targetType            string
	todoPretty            bool
	todoAll               bool
	addlabel              string
	removelabel           string
	fullmode              bool
	interest              bool
	indexPadding          int
	indexIDPadding        string
	palette               bool
	releasePlusRules      BZRule
	labelDefinitions      map[string]webhooks.Label
	regexLabelDefinitions map[string]webhooks.Label
	project               string
	username              string
	remote                string
	userBugzillas         []string
	nofetch               bool
)

func todoList(args []string) ([]*gitlab.Todo, error) {
	num, err := strconv.Atoi(todoNumRet)
	if todoAll || (err != nil) {
		num = -1
	}

	opts := gitlab.ListTodosOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: num,
		},
	}

	var lstr = strings.ToLower(todoType)
	switch {
	case lstr == "mr":
		targetType = "MergeRequest"
		opts.Type = &targetType
	case lstr == "issue":
		targetType = "Issue"
		opts.Type = &targetType
	}

	return lab.TodoList(opts, num)
}

func containsDate(holidays []time.Time, holiday time.Time) bool {
	for _, date := range holidays {
		if date == holiday {
			return true
		}
	}
	return false
}

func containsUserName(names []*gitlab.BasicUser, name string) bool {
	for _, a := range names {
		if a.Username == name {
			return true
		}
	}
	return false
}

func contains(names []string, name string) bool {
	for _, a := range names {
		if a == name {
			return true
		}
	}
	return false
}

func match(names []string, name string) int {
	for count, a := range names {
		if a == name {
			return count
		}
	}
	return -1
}

func common(a []string, b []string) (ret []string) {
	com := make(map[string]bool)

	for _, item := range a {
		com[item] = true
	}

	for _, item := range b {
		if _, ok := com[item]; ok {
			ret = append(ret, item)
		}
	}

	return ret
}

func truncateString(str string, num int) string {
	newStr := str
	if len(str) > num {
		if num > 3 {
			num -= 3
		}
		newStr = str[0:num] + "..."
	}
	return newStr
}

func getProjectTodos(args []string) (projectTodos []*gitlab.Todo) {
	if project != "" {
		fmt.Printf("Fetching %s Todos for %s...\n", username, project)
	}

	// create a list of Todos for THIS project
	todoAll = true
	todoType = "mr"
	todos, err := todoList(args)
	if err != nil {
		log.Fatal(err)
	}

	var (
		nameTodos  []string
		countTodos []int
	)

	for _, todo := range todos {
		index := match(nameTodos, todo.Project.Name)
		if index == -1 {
			nameTodos = append(nameTodos, todo.Project.Name)
			countTodos = append(countTodos, 0)
			index = len(nameTodos) - 1
		}
		countTodos[index]++

		if project != "" && todo.Project.PathWithNamespace == project && todo.TargetType == "MergeRequest" {
			projectTodos = append(projectTodos, todo)
		}
	}

	for n, name := range nameTodos {
		fmt.Printf("%s has %d Todos() in %s\n", username, countTodos[n], name)
	}

	return projectTodos
}

func getThisTodo(mrIID int, projectTodos []*gitlab.Todo) *gitlab.Todo {
	for _, todo := range projectTodos {
		if mrIID == todo.Target.IID {
			return todo
		}
	}
	return nil
}

func getIDsFromDescription(comp string, description string) ([]string, error) {
	var idStrs []string
	var _idStr string

	idExp, _ := regexp.Compile(fmt.Sprintf("%s.*", comp))
	_idStrs := idExp.FindAllString(description, -1)
	if len(_idStrs) == 0 {
		return nil, fmt.Errorf("No %s found in the MR description.", comp)
	}

	possibleError := false
	for _, idStr := range _idStrs {
		idStr = strings.TrimSpace(idStr)

		// It doesn't make sense to have an INTERNAL ticket with a normal
		// BZ.  Authors would just use the normal ticket.
		if strings.Contains(idStr, "INTERNAL") {
			idStrs = append(idStrs, "INTERNAL")
			return idStrs, nil
		}

		// These are all valid strings:
		// Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=1837350
		// Bugzilla: https://bugzilla.redhat.com/1837350
		// JIRA: https://issues.redhat.com/browse/RHEL-1234
		if strings.Contains(idStr, "bugzilla.redhat.com") {
			if strings.Contains(idStr, "=") {
				s := strings.Split(idStr, "=")
				_, err := strconv.Atoi(s[1])
				if err != nil {
					possibleError = true
					continue
				}
				idStrs = append(idStrs, s[1])
				continue
			}

			s := strings.Split(idStr, "/")
			_idStr = s[len(s)-1]
			_, err := strconv.Atoi(_idStr)
			if err != nil {
				possibleError = true
				continue
			}
		} else if strings.Contains(idStr, "issues.redhat.com") {
			s := strings.Split(idStr, "/")
			_idStr = s[len(s)-1]
		}

		idStrs = append(idStrs, _idStr)
		possibleError = false
	}

	if possibleError == true && len(idStrs) == 0 {
		return nil, fmt.Errorf("Invalid %s entry", comp)
	}
	return idStrs, nil
}

func getUsersSubsystemLabels() []string {
	fmt.Printf("Fetching %s labels for %s from owners.yaml...\n", username, project)
	ownersLabels, err := getOwnersLabels()
	if err != nil {
		log.Fatal(err)
	}

	if username != lab.User() {
		// cannot get remaining information for other users
		return ownersLabels
	}
	combinedLabels := ownersLabels

	fmt.Printf("Fetching %s labels for %s from GitLab...\n", username, project)
	var subscribedGitLabLabels []string
	var subscribedLabels []string
	// Cannot rely on GitLab built-in label support because of:
	// https://gitlab.com/gitlab-com/account-management/eastern-north-america/red-hat/red-hat-rhel-group-pov/-/issues/72
	gitLablabels, err := lab.LabelList(project)
	if err != nil {
		log.Fatal(err)
	}
	for _, label := range gitLablabels {
		if label.Subscribed {
			subscribedGitLabLabels = append(subscribedGitLabLabels, label.Name)
		}
	}

	// combine the labels in owners.yaml with the self-chosen ones in
	// the config and remove whitespace, and the ones reported by GitLab as
	// "subscribed"
	fmt.Printf("Fetching %s labels for %s from config...\n", username, project)
	combinedLabels = append(combinedLabels, config.GetSubscribedLabels()...)
	combinedLabels = append(combinedLabels, subscribedGitLabLabels...)

	// remove whitespaces from labels
	for count, label := range combinedLabels {
		combinedLabels[count] = strings.TrimSpace(label)
	}

	// Create a map of all unique elements.
	encountered := map[string]bool{}
	for v := range combinedLabels {
		encountered[combinedLabels[v]] = true
	}

	// This is the final list of labels from owners.yaml and subscribed labels
	for key, _ := range encountered {
		subscribedLabels = append(subscribedLabels, key)
	}

	var subsystemLabels []string
	for _, label := range subscribedLabels {
		label = strings.TrimSpace(label)
		if strings.Contains(label, "Subsystem") || label == "KABI" || label == "Configuration" || label == "External Contributors" {
			subsystemLabels = append(subsystemLabels, label)
		}
	}

	return subsystemLabels
}

func evaluateFlag(name string, value string, states []string, indent int) {
	for _, state := range states {
		if state == value {
			return
		}
	}
	indentLinef(indent, red, "- invalid %s flag (%s)\n", name, value)
}

func indentLine(padding int, color func(...interface{}) string, entry string) {
	if strings.TrimSpace(entry) == "" {
		return
	}

	words := strings.Split(entry, " ")
	lineWidth := termWidth - padding
	lineSize := 0

	fmt.Printf("%s", strings.Repeat(" ", padding+1))
	for _, w := range words {
		lineSize += len(w) + 1
		if lineSize >= lineWidth {
			fmt.Printf("\n%s", strings.Repeat(" ", padding+1))
			lineSize = len(w) + 1
		}
		fmt.Printf(color("%s "), w)
	}
	fmt.Printf("\n")
}

func indentLinef(padding int, color func(...interface{}) string, format string, args ...interface{}) {
	entry := fmt.Sprintf(format, args...)
	if strings.TrimSpace(entry) == "" {
		return
	}

	words := strings.Split(entry, " ")
	lineWidth := termWidth - padding
	lineSize := 0

	fmt.Printf("%s", strings.Repeat(" ", padding+1))
	for num, w := range words {
		lineSize += len(w) + 1
		if lineSize >= lineWidth {
			fmt.Printf("\n%s", strings.Repeat(" ", padding+1))
			lineSize = len(w) + 1
		}
		if num+1 == len(words) {
			fmt.Printf(color("%s"), w)
		} else {
			fmt.Printf(color("%s "), w)
		}
	}
}

func checkDTMandITM(issue *gojira.Issue, indentLen int, jiraStr string) {
	// verify DTM
	DTM, err := jira.GetDevTargetMilestone(issue)
	if err != nil {
		fmt.Println("Error: Could not read Dev Target Mileston from", jiraStr)
		log.Fatal(err)
	}
	if DTM == "" {
		// PRARIT: FIXME output Devel Engineer's name?
		indentLinef(indentLen, red, "- %s: DTM must be set by Devel Engineer\n", jiraStr)
	}
	// verify ITM
	ITM, err := jira.GetInternalTargetMilestone(issue)
	if err != nil {
		fmt.Println("Error: Could not read Internal Target Milestone from", jiraStr)
		log.Fatal(err)
	}
	if ITM == "" {
		// PRARIT: FIXME output QE Engineer's name?
		indentLinef(indentLen, red, "- %s: ITM must be set by QE Engineer\n", jiraStr)
	}
}

func checkFixVersions(issue *gojira.Issue, jiraFixVersions string, indentLen int, jiraStr string) {
	// verify FixVersions
	if jiraFixVersions != "" {
		fixVersion, err := jira.GetFixVersions(issue)
		if err != nil {
			fmt.Println("Error: Could not read Fix Version/s from", jiraStr)
			log.Fatal(err)
		}
		if fixVersion != jiraFixVersions {
			indentLinef(indentLen, red, "- %s: Fix Version/s (%s) is not %s\n", jiraStr, fixVersion, jiraFixVersions)
		}
	}
}

// Jira::Planning indicates that the Jira ticket is missing one of DTM, ITM, fixVersions, an MR link,
// or that the https://pkgs.devel.redhat.com/rules.html rules don't apply properly
func decipherJiraPlanning(mr *gitlab.MergeRequest, indent int) {
	mrIID := strconv.Itoa(mr.IID)
	mrMilestone = ""
	if mr.Milestone != nil {
		mrMilestone = mr.Milestone.Title
	}

	indentStatus := indent + len("- status: ")
	indentJiraErr := indentStatus + len("Jira needs review: ")

	jiraStrs, err := getIDsFromDescription("JIRA:", mr.Description)
	if err != nil {
		// in theory this should never happen
		indentLine(indent, red, "- status: Invalid Jira: entry in MR Description.  Fix the MR description.")
		indentLinef(indentStatus, red, "ex) lab mr edit %s\n", mrIID)
		return
	}

	for _, jiraStr := range jiraStrs {
		// INTERNAL Jiras have no "real" Jira.  I'm not sure that
		// JIRA::Planning can be applied to an INTERNAL Jira.
		if strings.Contains(jiraStr, "INTERNAL") {
			indentLine(indent, red, "- status: Jira::NeedsReview: INTERNAL Jira specified")
			continue
		}

		// get the state of the Jira ticket
		issue, err := jira.GetIssue(jiraStr)
		if err != nil {
			// PRARIT: FIXME this doesn't need to be fatal and can be a returned
			log.Fatal(err)
		}

		fixVersion, err := jira.GetFixVersions(issue)
		if err != nil {
			indentLinef(indentJiraErr, red, "- %s: Set Fix Version and rerun rhstatus.\n", jiraStr)
			indentLinef(indentJiraErr, red, "- rhstatus cannot determine MR state without a jira Fix Version.\n")
			continue
		}

		// compare mrMilestone to fixVersion
		if fixVersion != strings.ToLower(mrMilestone) {
			indentLinef(indentJiraErr, red, "- Jira FixVersion/s(%s) does not match Gitlab !%s Milestone(%s).\n", fixVersion, mrIID, mrMilestone)
		}

		// get the package rule from https://pkgs.devel.redhat.com/rules.html
		infixString := getPkgsDevelRule(strings.Trim(mrMilestone, "RHEL-"))
		// convert to PostFix
		postFix := postfix.ToLogicalPostfix(infixString)
		// get the jira variables and values
		jiraVariables := returnJiraEntries(postFix, false)
		// known jira variables returned from above call
		// jiraProject
		// jiraField(fixVersions)

		// PRARIT: FIXME verify project is RHEL or is that silly?

		// verify FixVersions
		checkFixVersions(issue, jiraVariables["fixVersions"], indentJiraErr, jiraStr)
		// check DTM and ITM
		checkDTMandITM(issue, indentJiraErr, jiraStr)
		// verify Docs Impact (optional)
		if jiraVariables["docsImpact"] != "" {
			docsImpact, err := jira.GetDocsImpact(issue)
			if err != nil {
				fmt.Println("Error: Could not read Docs Impact from", jiraStr)
				log.Fatal(err)
			}
			if docsImpact != jiraVariables["docsImpacts"] {
				indentLinef(indentJiraErr, red, "- %s: Docs Impact (%s) is not %s\n", jiraStr, fixVersion, jiraVariables["docsImpacts"])
			}
		}
	}
}

// Jira::InProgress indicates that the Jira ticket has all it's fields correctly filled out (DTM, ITM, etc.),
// and rules, but it can't hurt to double check the fixVersions and Milestone again.  This state should check
// to see what the testing status is.
func decipherJiraInProgress(mr *gitlab.MergeRequest, indent int) {
	mrIID := strconv.Itoa(mr.IID)
	mrMilestone = ""
	if mr.Milestone != nil {
		mrMilestone = mr.Milestone.Title
	}

	indentStatus := indent + len("- status: ")
	indentJiraErr := indentStatus + len("Jira needs review: ")

	jiraStrs, err := getIDsFromDescription("JIRA:", mr.Description)
	if err != nil {
		// in theory this should never happen
		indentLine(indent, red, "- status: Invalid Jira: entry in MR Description.  Fix the MR description.")
		indentLinef(indentStatus, red, "ex) lab mr edit %s\n", mrIID)
		return
	}

	for _, jiraStr := range jiraStrs {
		// INTERNAL Jiras have no "real" Jira.  I'm not sure that
		// JIRA::Planning can be applied to an INTERNAL Jira.
		if strings.Contains(jiraStr, "INTERNAL") {
			indentLine(indent, red, "- status: Jira::NeedsReview: INTERNAL Jira specified")
			continue
		}

		// get the state of the Jira ticket
		issue, err := jira.GetIssue(jiraStr)
		if err != nil {
			// PRARIT: FIXME this doesn't need to be fatal and can be a returned
			log.Fatal(err)
		}

		fixVersion, err := jira.GetFixVersions(issue)
		if err != nil {
			indentLinef(indentJiraErr, red, "- %s: Set Fix Version and rerun rhstatus.\n", jiraStr)
			indentLinef(indentJiraErr, red, "- rhstatus cannot determine MR state without a jira Fix Version.\n")
			continue
		}

		// compare mrMilestone to fixVersion
		if fixVersion != strings.ToLower(mrMilestone) {
			indentLinef(indentJiraErr, red, "- Jira FixVersion/s(%s) does not match Gitlab !%s Milestone(%s).\n", fixVersion, mrIID, mrMilestone)
		}

		// check MR link
		if !jira.CheckGitLabLink(issue, mr.WebURL) {
			indentLinef(indentJiraErr, red, "- %s: Warning: The MR and its artifacts are not linked to the Jira.\n", jiraStr)
		}
		// check Preliminary Testing: Requested
		// check Preliminary Testing: Passed
		prelimTesting, err := jira.GetPreliminaryTesting(issue)
		if err != nil {
			return
		}
		if prelimTesting == "Requested" {
			indentLinef(indentJiraErr, red, "- %s: Preliminary Testing has been requested of QE.\n", jiraStr)
		}

		if prelimTesting == "Passed" {
			indentLinef(indentJiraErr, red, "- %s: Preliminary Testing has been successfully completed by QE.\n", jiraStr)
		}
	}
}

func decipherAuthorLabels(mr *gitlab.MergeRequest) {
	mrIID := strconv.Itoa(mr.IID)
	mrMilestone = ""
	if mr.Milestone != nil {
		mrMilestone = mr.Milestone.Title
	}

	indent := len(noRole) + len(indexIDPadding) + 1

	// optimize readyForMerge.  If this label is on the MR, then there is
	// nothing for the dev engineer to do.  Just print a message and return.
	if contains(mr.Labels, "readyForMerge") {
		indentLine(indent, green, "- status: readyForMerge")
		indentLinef(indent+len("- status: "), green, "%s\n", labelDefinitions["readyForMerge"].Description)
		return
	}

	readyForQA := false
	if contains(mr.Labels, "readyForQA") {
		readyForQA = true
	}

	// output labels
	color := green
	for _, label := range mr.Labels {
		if strings.Contains(label, "::OK") || strings.Contains(label, "Subsystem:") || strings.Contains(label, "CodeChanged::") || label == "Bugzilla::NeedsTesting" || label == "readyForQA" || label == "Configuration" || label == "No CCs" {
			continue
		}
		if label == "JIRA::InProgress" && readyForQA {
			continue
		}
		if label == "CKI_RT::Failed::merge" {
			continue
		}
		if label == "TargetedTestingMissing" {
			continue
		}
		color = red
		break
	}
	fmt.Printf(color("%s %s - labels: "), noRole, indexIDPadding)

	labelPadding := termWidth - (indent + len(" - labels: "))

	comma := green(", ")
	remainingPadding := labelPadding
	for cnt, label := range mr.Labels {
		if cnt != 0 {
			fmt.Printf(comma)
		}
		// the -2 at the end is for the commas
		remainingPadding = remainingPadding - len(label) - 2
		if remainingPadding <= 0 {
			fmt.Printf("\n%s", strings.Repeat(" ", termWidth-labelPadding))
			// the -2 at the end is for the commas
			remainingPadding = labelPadding - len(label) - 2
		}
		if strings.Contains(label, "::OK") {
			fmt.Printf("%s", green(label))
			comma = green(", ")
		} else if strings.Contains(label, "Subsystem:") || label == "KABI" {
			fmt.Printf("%s", grey(label))
			comma = grey(", ")
		} else if strings.Contains(label, "CodeChanged::") {
			fmt.Printf("%s", grey(label))
			comma = green(", ")
		} else if label == "Bugzilla::NeedsTesting" || label == "readyForQA" || label == "Configuration" || label == "No CCs" {
			fmt.Printf("%s", green(label))
			comma = green(", ")
		} else if label == "CKI_RT::Failed::merge" || label == "TargetedTestingMissing" {
			// CKI_RT::Failed::merge can be ignored for now
			// TargetedTestingMissing can be ignored for now
			fmt.Printf("%s", green(label))
			comma = green(", ")
		} else if label == "JIRA::InProgress" && readyForQA {
			// readyForQA means everything is technically 'good' and there's
			// nothing for the user to do.
			fmt.Printf("%s", green(label))
			comma = green(", ")
		} else {
			fmt.Printf("%s", red(label))
			comma = red(", ")
		}
	}
	fmt.Printf("\n")

	for _, label := range mr.Labels {
		// skip if the label has ::OK Subsystem:, CodeChanged:, or Dependencies:
		if strings.Contains(label, "::OK") || strings.Contains(label, "Subsystem:") || strings.Contains(label, "CodeChanged:") || strings.Contains(label, "Dependencies:") || strings.Contains(label, "Configuration") {
			continue
		}

		if label == "JIRA::InProgress" && readyForQA {
			continue
		}

		if label == "TargetedTestingMissing" {
			continue
		}

		labelName := label

		if labelDefinitions[label].Name != label {
			// does this label match one of the regex labels?
			for _, regexLabel := range regexLabelDefinitions {
				regEx, _ := regexp.Compile(regexLabel.Name)
				_match := regEx.FindAllString(label, -1)
				if len(_match) > 0 {
					labelName = regexLabel.Name
				}
			}
		}

		color := red
		// if there is no developer action, the status is green
		if labelDefinitions[labelName].DevAction == "" || label == "Bugzilla::NeedsTesting" {
			color = green
		}

		indentLinef(indent, color, "- status: %s:\n", label)
		statusIndent := indent + len("- status: ")
		if labelDefinitions[labelName].Regex {
			fields := strings.Split(label, "::")
			if labelDefinitions[labelName].RegexFieldID > 0 {
				indentLinef(statusIndent, color, labelDefinitions[labelName].Description+"  "+labelDefinitions[labelName].DevAction+"\n", fields[labelDefinitions[labelName].RegexFieldID-1])
			}
		} else {
			indentLinef(statusIndent, color, "%s  %s\n", labelDefinitions[labelName].Description, labelDefinitions[labelName].DevAction)
		}

		//
		// Add extra rules and output here.
		//

		switch labelName {
		case "readyForQA":
			// do not output BZ status for "readyForQA".  If the BZ
			// statuses are not "Verified:Tested", then the
			// "Bugzilla::NeedsTested" label will be applied.  The BZ
			// statuses will be output for that label.
			jStrs, err := getIDsFromDescription("JIRA:", mr.Description)
			if err != nil {
				indentLine(indent, red, "- status: No valid JIRA entry in MR Description.  Fix the MR description.")
				indentLinef(indent, red, "  ex) lab mr edit %s\n", mrIID)
				return
			}
			if len(jStrs) == 0 {
				// this is currently valid as there may be BZs or Jiras, or both.
				continue
			}

			for _, jStr := range jStrs {
				if strings.Contains(jStr, "INTERNAL") {
					indentLine(indent, green, "- status: INTERNAL JIRA specified\n")
					continue
				}
				for _, l := range mr.Labels {
					if strings.HasPrefix(l, "Dependencies::") && l != "Dependencies::OK" {
						commits := strings.Split(l, "::")
						indentLinef(statusIndent, green, "This MR blocked by unmerged code at commit %s.\n", commits[1])
					}
				}

				issue, err := jira.GetIssue(jStr)
				if err != nil {
					// PRARIT: FIXME this doesn't need to be fatal and can be a returned
					log.Fatal(err)
				}
				prelimTesting, err := jira.GetPreliminaryTesting(issue)
				if err != nil {
					fmt.Println("Error: could not read Preliminary Testing from", jStr)
					log.Fatal(err)
				}
				if prelimTesting == "Requested" {
					targetEndStr, err := jira.GetTargetEnd(issue)
					if err != nil {
						log.Fatal(err)
					}
					targetEnd, err := time.Parse("2006-01-02", targetEndStr)
					timeNow := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)
					hours := int(targetEnd.Sub(timeNow).Hours() / 24)
					if hours > 0 {
						indentLinef(statusIndent, green, "- %s: Preliminary Testing: Request (results deadline: %d days)\n", jStr, int(targetEnd.Sub(timeNow).Hours()/24))
					} else if hours == 0 {
						indentLinef(statusIndent, green, "- %s: Preliminary Testing: Request (results deadline: today)\n", jStr)
					} else {
						qaContact, err := jira.GetQAContactInformation(issue)
						if err != nil {
							log.Fatal(err)
						}
						indentLinef(statusIndent, red, "- %s: Preliminary Testing: Request (results deadline missed by %d days).  Contact %s for additional information.\n", jStr, -int(targetEnd.Sub(timeNow).Hours()/24), qaContact)
					}
				}

				if prelimTesting == "Passed" {
					indentLinef(statusIndent, green, "- %s: Preliminary Testing has been successfully completed by QE.\n", jStr)
				}

			}
		case "Acks::Blocked":
			indentLine(statusIndent, red, "See the MR for details.")
			indentLinef(statusIndent, red, "ex) lab mr show %s --comments\n", mrIID)
		case "Acks::NACKed":
			indentLine(statusIndent, red, "See the MR for details.")
			indentLinef(statusIndent, red, "ex) lab mr show %s --comments\n", mrIID)
		case "Bugzilla::NeedsReview":
			// It seems that jsonrpc.cgi is broken somehow and I
			// will have to spend some time figuring out what is wrong.
			// For now, just omit this call.
			//decipherBZNeedsReview(mrIID, description, indent)
		case "Bugzilla::NeedsTesting":
			isPrinted := false

			bzStrs, err := getIDsFromDescription("Bugzilla:", mr.Description)
			if err != nil {
				indentLine(indent, red, "- status: No valid JIRA: or Bugzilla: entry in MR Description.  Fix the MR description.")
				indentLinef(indent, red, "  ex) lab mr edit %s\n", mrIID)
				return
			}

			for _, bzStr := range bzStrs {
				if strings.Contains(bzStr, "INTERNAL") {
					indentLine(indent, green, "- status: Bugzilla::NeedsReview: INTERNAL BZ specified\n")
					continue
				}

				for _, l := range mr.Labels {
					if strings.HasPrefix(l, "Dependencies::") && l != "Dependencies::OK" {
						commits := strings.Split(l, "::")
						indentLinef(statusIndent, green, "This MR blocked by unmerged code at commit %s.\n", commits[1])
					}
				}

				bzData := getBZdata(bzStr)
				if !contains(bzData.verified, "Tested") {
					if !isPrinted {
						indentLine(statusIndent, green, "These BZs require QE testing:")
						isPrinted = true
					}
					QAname, QAemail, OtherQA := getBugzillaQAContact(bzStr)
					indentLinef(statusIndent, green, " - https://bugzilla.redhat.com/%s\n", bzStr)
					if OtherQA {
						indentLinef(statusIndent+3, green, "QA contact: %s <%s> (OtherQA)\n", QAname, QAemail)
					} else {
						indentLinef(statusIndent+3, green, "QA contact: %s <%s>\n", QAname, QAemail)
					}
				}
			}

			if !isPrinted {
				deps, err := getIDsFromDescription("Depends:", mr.Description)
				if err != nil {
					continue
				}

				isPrinted := false
				for _, dep := range deps {
					bzData := getBZdata(dep)
					if !contains(bzData.verified, "Tested") {
						if !isPrinted {
							indentLine(statusIndent, green, "Testing of the MR's changeset is blocked on dependencies.  See these BZs for details:")
							isPrinted = true
						}
						QAname, QAemail, OtherQA := getBugzillaQAContact(dep)
						indentLinef(statusIndent, green, " - https://bugzilla.redhat.com/%s\n", dep)
						if OtherQA {
							indentLinef(statusIndent+3, green, "QA contact: %s <%s> (OtherQA)\n", QAname, QAemail)
						} else {
							indentLinef(statusIndent+3, green, "QA contact: %s <%s>\n", QAname, QAemail)
						}
					}
				}
			}
		case "CommitRefs::NeedsReview":
			indentLine(statusIndent, red, "Review the \"Upstream Commit ID Readiness\" report.")
			indentLinef(statusIndent, red, "ex) lab mr show %s ", mrIID)
			fmt.Printf(red("--comments\n"))
		case "Signoff::NeedsReview":
			indentLine(statusIndent, red, "Review the \"DCO Signoff Error(s)!\" report.")
			indentLinef(statusIndent, red, "ex) lab mr show %s ", mrIID)
			fmt.Printf(red("--comments\n"))
		case "CKI::NeedsReview":
			fmt.Printf(red(".  Debugging information can be found at:\n"))
			indentLine(statusIndent, red, "https://cki-project.org/docs/user_docs/gitlab-mr-testing/full_picture/#debugging-and-fixing-failures---more-details\n")
		case "CKI::Running":
			indentLinef(indent, red, "- status: CKI has not completed for MR %s\n", mrIID)
		case "JIRA::New":
			// Jira::New is a state that can only occur with a
			// draft MR.  Since it is draft we should have no
			// expectation of what the state of a Jira ticket is.
			// This is a no-op.  At some point in the future this
			// may change, so leave this function as a place holder
		case "JIRA::Planning":
			decipherJiraPlanning(mr, indent)
		case "JIRA::InProgress":
			decipherJiraInProgress(mr, indent)
		default:
		}
	}
}

func addLabel(label string) {
	labels := config.GetSubscribedLabels()
	if contains(labels, label) {
		fmt.Printf("Labels already contain %s ... no change made.\n", label)
		return
	}
	labels = append(labels, label)
	newlabels := strings.Join(labels, ",")

	config.MainConfig.Set("core.subscribedlabels", newlabels)
	config.MainConfig.WriteConfig()

	fmt.Printf("Labels set as: %s\n", newlabels)
}

func removeLabel(label string) {
	labels := config.GetSubscribedLabels()
	if !contains(labels, label) {
		fmt.Printf("Labels do not contain %s ... no change made.\n", label)
		return
	}

	newlabels := ""
	comma := false
	for _, _label := range labels {
		if _label == label {
			continue
		}
		if comma == true {
			newlabels = newlabels + ","
		}
		newlabels = newlabels + _label
		comma = true
	}

	config.MainConfig.Set("core.subscribedlabels", newlabels)
	config.MainConfig.WriteConfig()

	fmt.Println("Labels set as:", newlabels)
}

// getAckStartDate() returns the date that the MR Required Approval timeout
// starts at, and the date that the MR was assigned to the user.
// It will look for the last time the MR was placed into the Ready state, and if that
// doesn't exist, it returns date that the MR was added as a Todo item, or
// finally date that the MR was created.
//
// Readers might ask the question "What if someone adds me as a reviewer?" and realize
// this code doesn't take that into account.  That's correct -- it doesn't.  If you are
// manually added by an author or other participant, you are NOT a required approver.
func getAckStartDate(mr *gitlab.MergeRequest, discussions []*gitlab.Discussion, todo *gitlab.Todo) (*time.Time, *time.Time, []*gitlab.Discussion) {

	var ackDate *time.Time
	var assignDate *time.Time

	_assignDate := time.Now()
	_assignDate = _assignDate.AddDate(1, 0, 0)
	assignDate = &_assignDate

	if discussions == nil {
		var err error
		discussions, err = lab.MRListDiscussions(project, mr.IID)
		if err != nil {
			log.Fatal(err)
		}
	}
	// Find the last date that the MR was marked as Ready, and the last date
	// that the user was assigned to the MR
	for _, discussion := range discussions {
		for _, note := range discussion.Notes {
			if note.System {
				if note.Body == "marked this merge request as **ready**" {
					ackDate = note.UpdatedAt
				}
				if note.Body == ("assigned to @" + username) {
					assignDate = note.UpdatedAt
				}
			}
		}
	}
	if ackDate != nil {
		return ackDate, assignDate, discussions
	}

	// Return the date that the MR was added as a Todo item
	if todo != nil {
		return todo.CreatedAt, assignDate, discussions
	}
	// If that doesn't exist, return the date that the MR was created
	return mr.CreatedAt, assignDate, discussions
}

func getAckEndDate(mr *gitlab.MergeRequest, discussions []*gitlab.Discussion, todo *gitlab.Todo) (bool, int, time.Time, []*gitlab.Discussion) {
	var (
		ackStartDate *time.Time
		assignDate   *time.Time
	)

	holidays := config.GetHolidays()
	ackStartDate, assignDate, discussions = getAckStartDate(mr, discussions, todo)
	ackEndDate := ackStartDate.Round(24 * time.Hour)
	daysAdded := 0
	days := 0
	for {
		days++
		ackEndDate = ackEndDate.Add(time.Hour * 24)
		weekday := ackEndDate.Weekday().String()
		if weekday == "Sunday" || weekday == "Saturday" {
			continue
		}
		if containsDate(holidays, ackEndDate) {
			continue
		}
		daysAdded++
		if daysAdded == 10 {
			if assignDate.After(ackEndDate) {
				return true, int((time.Until(ackEndDate)).Hours()) / 24, ackEndDate, discussions
			}
			break
		}
	}
	return false, -1, ackEndDate, discussions
}

func addBlockers(ID int, username string, blockers map[string][]int) {
	blockers[username] = append(blockers[username], ID)
}

func isMRBlocked(mr *gitlab.MergeRequest, discussions []*gitlab.Discussion) (map[string][]int, []*gitlab.Discussion) {

	blockers := make(map[string][]int)
	if mr.BlockingDiscussionsResolved {
		return blockers, discussions
	}

	if discussions == nil {
		var err error
		discussions, err = lab.MRListDiscussions(project, mr.IID)
		if err != nil {
			log.Fatal(err)
		}
	}

	// There are two types of blocking threads.  The first is a standalone
	// created by someone in the MR.  In that case the UpdatedAt and
	// CreatedAt times should be the same.  In the case of where someone
	// replies to a comment and creates a blocking thread, the CreatedAt
	// and UpdatedAt times will be different as the note's UpdatedAt time
	// will be updated to the newly created blocking comment's CreatedAt
	// date.   Hence, the complexity below...
	for _, discussion := range discussions {
		note0 := discussion.Notes[0]
		if note0.Resolvable == false || note0.Resolved == true {
			continue
		}
		if len(discussion.Notes) == 1 {
			if note0.Type == "DiscussionNote" || note0.Type == "DiffNote" {
				addBlockers(note0.ID, note0.Author.Username, blockers)
			}
			continue
		}
		for n, note := range discussion.Notes {
			if note0.Type != "DiscussionNote" && note0.Type != "DiffNote" {
				break
			}
			if time.Time(*note.UpdatedAt).Before(*note0.UpdatedAt) {
				addBlockers(note.ID, note.Author.Username, blockers)
				break
			}
			if (n + 1) == len(discussion.Notes) {
				// In this case there is no way to determine
				// WHO has the blocking thread.  Just use the
				// original author
				addBlockers(note0.ID, note0.Author.Username, blockers)
			}
		}
	}
	return blockers, discussions
}

func outputStatusLine(roleColor func(...interface{}) string, role string, mrIDcolor func(...interface{}) string, mrID string, titleColor func(...interface{}) string, title string, authorColor func(...interface{}) string, author string, bzColor func(...interface{}) string, description string) {

	if len(userBugzillas) == 0 {
		fmt.Printf("%s %s%s %s %s\n", roleColor(role), strings.Repeat(" ", indexPadding-len(mrID)), mrIDcolor(mrID), titleColor(title), authorColor(author))
		return
	}

	bzStrs, _ := getIDsFromDescription("Bugzilla:", description)
	for _, bz := range bzStrs {
		if contains(userBugzillas, bz) {
			fmt.Printf("%s %s%s %s %s\n", roleColor(role), strings.Repeat(" ", indexPadding-len(bz)), mrIDcolor(bz), titleColor(title), authorColor(author))
			// output the MR ID or URL?
			indentLinef(len(noRole)+len(indexIDPadding)+1, bzColor, "Merge Request: %s\n", mrID)
		}
	}
}

// rhstatusCmd represents the list command
var rhstatusCmd = &cobra.Command{
	Use:   "rhstatus [mrID]",
	Short: "RedHat merge request status",
	Long:  ``,
	Args:  cobra.MaximumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		if palette {
			showPalette()
			return
		}
		setPalette()

		if addlabel != "" {
			addLabel(addlabel)
			return
		}
		if removelabel != "" {
			removeLabel(removelabel)
			return
		}

		_project, mrID, err := parseArgsRemoteAndID(args)
		if err != nil {
			return
		}

		if remote == "" {
			project = _project
		} else {
			project, err = getRemoteName(remote)
			if err != nil {
				return
			}
		}

		var projectTodos []*gitlab.Todo
		if username == "" {
			username = lab.User()
			if nofetch == false {
				projectTodos = getProjectTodos(args)
			}
		}

		if project == "" {
			// this "empty" return allows rhstatus to return the
			// Todo list in a non-GitLab dir.
			return
		}

		subsystemLabels := []string{}
		if nofetch == false {
			subsystemLabels = getUsersSubsystemLabels()
		}

		// Get Merge Request data
		var mrs []*gitlab.MergeRequest
		fmt.Printf("Fetching %s Merge Requests for %s...\n", username, project)
		if mrID == 0 {
			mrAll = true
			mrs, err = mrList(args, project)
			if err != nil {
				log.Fatal(err)
			}
			// user may only want to look at specific bugzillas
			if len(userBugzillas) > 0 {
				var _mrs []*gitlab.MergeRequest
				for _, mr := range mrs {
					bzStrs, _ := getIDsFromDescription("Bugzilla:", mr.Description)
					for _, bz := range bzStrs {
						if contains(userBugzillas, bz) {
							_mrs = append(_mrs, mr)
							break
						}
					}
				}
				mrs = _mrs
			}
		} else {
			mr, err := lab.MRGet(project, int(mrID))
			if err != nil {
				log.Fatal(err)
			}
			mrs = append(mrs, mr)
		}

		fmt.Printf("Fetching webhook label definitions...\n")
		// Get webhook label definitions
		webhookDefs, err := webhooks.GetWebhooksLabelDefinitions()
		if err != nil {
			log.Fatal(err)
		}
		// translate the yaml into a go map for easier lookup
		labelDefinitions = make(map[string]webhooks.Label)
		for _, w := range webhookDefs.Labels {
			labelDefinitions[w.Name] = w
		}
		regexLabelDefinitions = make(map[string]webhooks.Label)
		for _, w := range webhookDefs.Labels {
			if w.Regex {
				regexLabelDefinitions[w.Name] = w
			}
		}

		pager := NewPager(cmd.Flags())
		defer pager.Close()

		if len(userBugzillas) == 0 {
			maxMR := 0
			for _, mr := range mrs {
				if mr.IID > maxMR {
					maxMR = mr.IID
				}
			}
			indexPadding = len(strconv.Itoa(maxMR))
		} else {
			maxBZ := 0
			for _, bz := range userBugzillas {
				if len(bz) > maxBZ {
					maxBZ = len(bz)
				}
			}
			indexPadding = maxBZ
		}

		indexIDPadding = strings.Repeat(" ", indexPadding)
		// This is used to calculate the displayed title with ellipses.
		// The "3" at the end is the number of spaces between columns
		// (ie, there are 3 columns with 2 spaces), and an extra -1 for
		// the EOL
		mrTitleTruncateLength := termWidth - len(noRole) - len(indexIDPadding) - 3

		for _, mr := range mrs {
			var (
				approvers            []string
				blockers             map[string][]int
				commonLabels         []string
				discussions          []*gitlab.Discussion
				role                 string = noRole
				roleColor            func(...interface{}) string
				roleDefined          bool = false
				isAuthor             bool = false
				isReviewer           bool = false
				isAssignee           bool = false
				isReqApprover        bool = false
				isApprovedByUser     bool = false
				unapprovedSubsystems []string
			)

			thisTodo := getThisTodo(mr.IID, projectTodos)

			mrIID := strconv.Itoa(mr.IID)

			displayAuthor := "(" + mr.Author.Username + ")"
			title := truncateString(mr.Title, mrTitleTruncateLength-len(displayAuthor)-1)

			if !fullmode && len(userBugzillas) == 0 {
				// Standard mode ONLY reports detailed status
				// for MRs that the user has a role.  ie) user
				// is a reviewer, an assignee, or an author.
				// Standard mode assumes that the ACK/NACK bot
				// has done it's magic and added a user to the
				// MR correctly.
				if !containsUserName(mr.Assignees, username) && !containsUserName(mr.Reviewers, username) && mr.Author.Username != username && thisTodo == nil {
					if interest {
						continue
					}
					// check for labels
					color := white
					if mr.WorkInProgress {
						color = grey
					}
					outputStatusLine(color, role, color, mrIID, color, title, color, displayAuthor, color, mr.Description)
					continue
				}
			}

			// check to see if the user is the author of the MR
			if mr.Author.Username == username {
				isAuthor = true
				roleDefined = true
			}

			if !isAuthor {
				if containsUserName(mr.Assignees, username) {
					isAssignee = true
					roleDefined = true
				}

				if containsUserName(mr.Reviewers, username) {
					isReviewer = true
					roleDefined = true
				}
			}

			// The rule examination below doesn't take into account
			// approvals done on MRs that a user has no role in.
			approvalConfig, err := lab.GetMRApprovalsConfiguration(project, mr.IID)
			if err != nil {
				log.Fatal(err)
			}
			for _, approvedby := range approvalConfig.ApprovedBy {
				approvers = append(approvers, approvedby.User.Username)
				if approvedby.User.Username == username {
					roleDefined = true
					isApprovedByUser = true
				}
			}

			// if user is the author then they can't be a required approver
			if !isAuthor {
				approvals, err := lab.GetMRApprovalState(project, mr.IID)
				if err != nil {
					log.Fatal(err)
				}

				// The approvals list provides rules for _each_ MR, not all
				// MRs.  Each rule in the approvals has it's own user list.
				// Do NOT use the eligible list here.  That list contains ALL
				// members, not just the ones for the rule.
				for _, rule := range approvals.Rules {
					for _, user := range rule.Users {
						if user.Username == username {
							roleDefined = true
							isReqApprover = true
							if !rule.Approved {
								unapprovedSubsystems = append(unapprovedSubsystems, rule.Name)
							}
						}
					}
				}
			}

			// Leave these lines for easier debug
			//fmt.Println("isAuthor", "isAssignee", "isReviewer", "isReqApprover", "isApprovedByUser", "len(unapprovedSubsystems)")
			//fmt.Println(isAuthor, isAssignee, isReviewer, isReqApprover, isApprovedByUser, len(unapprovedSubsystems))

			roleColor = green
			if roleDefined {
				if isAuthor {
					roleColor = yellow
					if mr.HasConflicts || !mr.BlockingDiscussionsResolved {
						roleColor = red
					}
					role = "author  "
				} else if isApprovedByUser {
					roleColor = green
					role = "approved"
				} else if isReviewer && !isReqApprover {
					roleColor = green
					role = "reviewer"
					// if the user has added their own Todo, mark it yellow
					if thisTodo != nil && thisTodo.Author.Username == lab.User() {
						roleColor = yellow
						role = "reviewer"
					}
				} else if isReqApprover && len(unapprovedSubsystems) != 0 {
					roleColor = red
					role = "approver"
				} else if isReqApprover && len(unapprovedSubsystems) == 0 {
					roleColor = green
					role = "approver"
				} else if isAssignee {
					roleColor = green
					role = "assignee"
					// if the user has added their own Todo, mark it yellow
					if thisTodo != nil && thisTodo.Author.Username == lab.User() {
						roleColor = yellow
						role = "assignee"
					}
				}
			} else {
				// check to see if this MR's labels match the user's labels
				commonLabels = common(subsystemLabels, mr.Labels)
				if commonLabels != nil {
					// This is considered an Unassigned state because
					// the user is not a author, reviewer, or assignee.
					// It should be highlighted
					roleColor = green
					role = "labels  "
				}
			}

			// check to see if this MR is on the user's Todo list
			mrIIDColor := white
			if thisTodo != nil {
				if !roleDefined {
					// todo list isn't a real role
					roleColor = green
					role = "todo    "
				}
				mrIIDColor = BLUE
			}

			// if this MR is a draft, then grey it out
			if mr.WorkInProgress {
				title = grey(title)
				displayAuthor = grey(displayAuthor)
				if thisTodo == nil {
					mrIIDColor = grey
				}
			}

			blockers, discussions = isMRBlocked(mr, discussions)
			if len(blockers[username]) > 0 {
				roleColor = red
			}

			bzColor := roleColor
			if mr.WorkInProgress {
				bzColor = grey
			}

			// output basic information (status, MR, and Title)
			outputStatusLine(roleColor, role, mrIIDColor, mrIID, white, title, white, displayAuthor, bzColor, mr.Description)

			// List MR labels for MRs that the user is interested in.
			// This only happens if the user does not have a role on
			// the MR (see above)
			if !roleDefined && commonLabels != nil {
				fmt.Printf(red("%s %s - MR %s "), noRole, indexIDPadding, mrIID)
				fmt.Printf(red("has labels: "))
				for cnt, label := range commonLabels {
					if cnt != 0 {
						fmt.Printf(red(", "))
					}
					fmt.Printf(red("%s"), label)
				}
				fmt.Printf("\n")
				fmt.Printf(red("%s %s   If you want to review, update the MR with:\n"), noRole, indexIDPadding)
				fmt.Printf(red("%s %s   ex) lab mr edit %s "), noRole, indexIDPadding, mrIID)
				fmt.Printf(red("--review %s\n"), username)
			}

			// list any existing approvers
			if roleDefined && approvers != nil && !isApprovedByUser && !mr.WorkInProgress {
				fmt.Printf("%s %s - approved by: ", noRole, indexIDPadding)
				for cnt, approver := range approvers {
					if cnt != 0 {
						fmt.Printf(", ")
					}
					fmt.Printf("%s", approver)
				}
				fmt.Printf("\n")
			}

			// if the user is the author, output additional information
			// about the labels
			if isAuthor {
				mrMilestone = ""
				if mr.Milestone != nil {
					mrMilestone = mr.Milestone.Title
				}
				decipherAuthorLabels(mr)
				if mr.HasConflicts {
					fmt.Printf(red("%s %s - MR %s has conflicts and needs to be rebased\n"), noRole, indexIDPadding, mrIID)
				}
			}

			if len(blockers) > 0 {
				for busername := range blockers {
					numblockers := len(blockers[busername])
					if numblockers == 1 {
						fmt.Printf(red("%s %s - MR %s "), noRole, indexIDPadding, mrIID)
						fmt.Printf(red("is blocked by %s and needs to be resolved\n"), busername)
					} else {
						fmt.Printf(red("%s %s - MR %s "), noRole, indexIDPadding, mrIID)
						fmt.Printf(red("has %d blocking threads from %s which need to be resolved\n"), numblockers, busername)
					}

					if username != busername {
						continue
					}

					fmt.Printf(red("%s %s   After review the MR can be resolved with\n"), noRole, indexIDPadding)
					for _, id := range blockers[busername] {

						fmt.Printf(red("%s %s    ex)  lab mr reply %s:%d --resolve\n"), noRole, indexIDPadding, mrIID, id)
					}
				}
			}

			// calculate the Acknowledgement Deadline which is 10 business
			// days from notification
			if !isAuthor && roleDefined && !isApprovedByUser && !mr.WorkInProgress {
				var (
					output     bool
					daysLeft   int
					ackEndDate time.Time
				)
				output, daysLeft, ackEndDate, discussions = getAckEndDate(mr, discussions, thisTodo)
				if !output {
					continue
				}

				if daysLeft < 0 {
					fmt.Printf(red("%s %s - status: Acknowledgement date was missed (%s)\n"), noRole, indexIDPadding, ackEndDate.Format("2006-01-02"))
				} else {
					fmt.Printf(red("%s %s - status: Acknowledgement Deadline days left: %d (%s)\n"), noRole, indexIDPadding, daysLeft, ackEndDate.Format("2006-01-02"))
				}
			}

			// Check to see if this MR can be removed from the Todo list
			// because the user does not have a role
			if thisTodo != nil && !roleDefined {
				if thisTodo.Author.Username != username {
					fmt.Printf("%s %s - MR %s seems to no longer need your attention and may be removed\n", noRole, indexIDPadding, mrIID)
					fmt.Printf("%s %s   from your Todo list.\n", noRole, indexIDPadding)
					fmt.Printf("%s %s   ex) lab todo done %d \n", noRole, indexIDPadding, thisTodo.ID)
				}
			}

			// Check to see if this MR can be removed from the Todo list
			// because the user has approved the MR
			if thisTodo != nil && isApprovedByUser {
				fmt.Printf("%s %s - MR %s is approved and can be removed from your Todo List.\n", noRole, indexIDPadding, mrIID)
				fmt.Printf("%s %s   ex) lab todo done %d \n", noRole, indexIDPadding, thisTodo.ID)
			}
		}
	},
}

func init() {
	rhstatusCmd.Flags().StringVar(&mrAuthor, "author", "", "list only MRs authored by $username")
	rhstatusCmd.Flags().StringVar(
		&mrAssignee, "assignee", "", "list only MRs assigned to $username")
	rhstatusCmd.Flags().StringVar(&mrOrder, "order", "updated_at", "display order (updated_at/created_at)")
	rhstatusCmd.Flags().StringVar(&mrSortedBy, "sort", "desc", "sort order (desc/asc)")
	rhstatusCmd.Flags().BoolVarP(&mrDraft, "draft", "", false, "list MRs marked as draft")
	rhstatusCmd.Flags().BoolVarP(&mrReady, "ready", "", false, "list MRs not marked as draft")
	rhstatusCmd.Flags().SortFlags = false
	rhstatusCmd.Flags().StringVar(
		&mrReviewer, "reviewer", "", "list only MRs with reviewer set to $username")
	rhstatusCmd.Flags().StringVar(&addlabel, "add-label", "", "Add a label to rhstatus' watch list")
	rhstatusCmd.Flags().StringVar(&removelabel, "remove-label", "", "Remove a label from rhstatus' watch list")
	rhstatusCmd.Flags().BoolVarP(&fullmode, "full", "", false, "Analyze all MRs")
	rhstatusCmd.Flags().BoolVarP(&interest, "interest", "", false, "Only report MRs that are of interest to the user")
	rhstatusCmd.Flags().StringVar(&username, "user", "", "Specify a user (does not display Todo list or subscribed labels")
	rhstatusCmd.Flags().BoolVarP(&palette, "palette", "", false, "Show .config palette colors. (See README.md for instructions)")
	rhstatusCmd.Flags().StringVar(&remote, "remote", "", "Specify a remote other than origin")
	rhstatusCmd.Flags().StringSliceVarP(&userBugzillas, "bugzilla", "", []string{}, "Specify a comma-separated list of bugzillas for output.")
	rhstatusCmd.Flags().BoolVarP(&nofetch, "nofetch", "", false, "Do not fetch Todos or labels from GitLab.")
}
