package cmd

import (
	"time"
)

type BZdataCF struct {
	Deadline string   `json:"cf_deadline,omitempty"`                  // use bzData.deadline, unset is ""
	DevTM    string   `json:"cf_dev_target_milestone,omitempty"`      // unset is ""
	ITM      string   `json:"cf_internal_target_milestone,omitempty"` // unset is ""
	ZTR      string   `json:"cf_zstream_target_release,omitempty"`    // unset is ""
	ITR      string   `json:"cf_internal_target_release,omitempty"`   // unset is "---"
	Verified []string `json:"cf_verified,omitempty"`                  // unset is ""
}

type BZCustomFields struct {
	Bugs []BZdataCF `json:"bugs"`
}

type BZdata struct {
	deadline time.Time // Use this one.  Do not use customFields.Deadline.
	b        map[string]string
	verified []string
}

type BZRuleQuery struct {
	Bugzilla_api_key string `json:"Bugzilla_api_key"`
	Names            string `json:"names"`
}

type BZRuleMatchWordAndValues struct {
	Word   string   `json:"word,omitempty"`
	Values []string `json:"values,omitempty"`
}

type BZRuleMatchStatusAndName struct {
	Status []string `json:"status,omitempty"`
	Name   string   `json:"name,omitempty"`
}

type BZRuleMatchJson struct {
	BugStatus               BZRuleMatchWordAndValues   `json:"bug_status,omitempty"`
	InternalTargetMilestone BZRuleMatchWordAndValues   `json:"cf_internal_target_milestone,omitempty"`
	InternalTargetRelease   BZRuleMatchWordAndValues   `json:"cf_internal_target_release,omitempty"`
	Classification          BZRuleMatchWordAndValues   `json:"classification,omitempty"`
	Product                 BZRuleMatchWordAndValues   `json:"product,omitempty"`
	FlagTypes               []BZRuleMatchStatusAndName `json:"flag_types,omitempty"`
}

type BZRuleActionJsonFlagTypesRelease struct {
	Status    string `json:"status,omitempty"`
	Ignore    int    `json:"ignore,omitempty"`
	Condition string `json:"condition,omitempty"`
}

type BZRuleActionJsonFlagTypes struct {
	Release BZRuleActionJsonFlagTypesRelease `json:"release,omitempty"`
}

type BZRuleActionJson struct {
	FlagTypes BZRuleActionJsonFlagTypes `json:"flag_types,omitempty"`
}

type BZRuleDetails struct {
	MatchJson  BZRuleMatchJson  `json:"match_json,omitempty"`
	ActionJson BZRuleActionJson `json:"action_json,omitempty"`
}

type BZRuleResult struct {
	Usage_7days    int           `json:"usage_7days,omitempty"`
	Usage_24hrs    int           `json:"usage24hrs,omitempty"`
	Definition     string        `json:"definition,omitempty"`
	Owners         []string      `json:"owners,omitempty"`
	BZRuleGroupId  int           `json:"rule_group_id,omitempty"`
	UseSinceChange int           `json:"use_since_change,omitempty"`
	MinorUpdate    bool          `json:"minor_update,omitempty"`
	Id             int           `json:"id,omitempty"`
	useCount       int           `json:"use_count,omitempty"`
	Name           string        `json:"name,omitempty"`
	LastModified   string        `json:"last_modified,omitempty"`
	Description    string        `json:"description,omitempty"`
	Details        BZRuleDetails `json:"details,omitempty"`
	IsPeriodic     bool          `json:"is_periodic,omitempty"`
	IsActive       bool          `json:"is_active,omitempty"`
	LastUsed       string        `json:"last_used,omitempty"`
}

type BZRuleError struct {
	Code    int    `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

type BZRule struct {
	Error  BZRuleError    `json:"error,omitempty"`
	Id     string         `json:"id,omitempty"`
	Result []BZRuleResult `json:"result,omitempty"`
}
